﻿using Dapper;
using latihan_netcore.Models;
using latihan_netcore.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Data;


namespace latihan_netcore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : Controller
    {
        private readonly IProduct _authentication;
        public ProductsController(IProduct authentication)
        {
            _authentication = authentication;
        }

        [Microsoft.AspNetCore.Mvc.HttpGet("ProductList")]
        [Authorize(Roles = "Admin")]
        public IActionResult getProduct()
        {
            var result = _authentication.getProductList<ModelProduct>();

            return Ok(result);
        }

        [Microsoft.AspNetCore.Mvc.HttpPost("CreateProduct")]
        [Authorize(Roles = "Admin")]
        public IActionResult CreateProduct([System.Web.Http.FromBody] ModelProduct product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("idcompany", product.IDCompany, DbType.String);
            dp_param.Add("idbrand", product.IDBrand, DbType.String);
            dp_param.Add("name", product.Name, DbType.String);
            dp_param.Add("varian", product.Varian, DbType.String);
            dp_param.Add("price", product.Price, DbType.String);
            dp_param.Add("iduser", product.CreatedBy, DbType.String);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelUser>("sp_CreateProduct", dp_param);
            if (result.Code == 200)
            {
                return Ok(result);
            }

            return BadRequest(result);
        }

        [Microsoft.AspNetCore.Mvc.HttpPut("Update Product")]
        [Authorize(Roles = "Admin")]
        public IActionResult Update([System.Web.Http.FromBody] ModelProduct product, string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("id", id, DbType.String);
            dp_param.Add("idcompany", product.IDCompany, DbType.String);
            dp_param.Add("idbrand", product.IDBrand, DbType.String);
            dp_param.Add("name", product.Name, DbType.String);
            dp_param.Add("varian", product.Varian, DbType.String);
            dp_param.Add("price", product.Price, DbType.String);
            dp_param.Add("iduser", product.CreatedBy, DbType.String);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelBrand>("sp_UpdateProduct", dp_param);
            if (result.Code == 200)
            {
                return Ok(new { data = product });
            }

            return BadRequest(result);
        }

        [Microsoft.AspNetCore.Mvc.HttpDelete("Delete")]
        [Authorize(Roles = "Admin")]
        public IActionResult Delete(string id)
        {
            if (id == string.Empty)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("ID", id, DbType.String);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelBrand>("sp_DeleteProduct", dp_param);

            if (result.Code == 200)
            {
                return Ok(result);
            }

            return NotFound(result);
        }

    }
}
