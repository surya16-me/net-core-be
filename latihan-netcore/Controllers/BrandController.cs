﻿using Dapper;
using latihan_netcore.Models;
using latihan_netcore.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Data;

namespace latihan_netcore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BrandController : Controller
    {
        private readonly IJWTAuthManager _authentication;
        public BrandController(IJWTAuthManager authentication)
        {
            _authentication = authentication;
        }

        [Microsoft.AspNetCore.Mvc.HttpGet("BrandList")]
        [Authorize(Roles = "Admin")]
        public IActionResult getBrand()
        {
            var result = _authentication.getBrandList<ModelBrand>();

            return Ok(result);
        }


        [Microsoft.AspNetCore.Mvc.HttpPost("Create")]
        [Authorize(Roles = "Admin")]
        public IActionResult CreateBrand([System.Web.Http.FromBody] ModelBrand brand)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("name", brand.Name, DbType.String);
            dp_param.Add("address", brand.Address, DbType.String);
            dp_param.Add("telephone", brand.Telephone, DbType.String);
            dp_param.Add("email", brand.Email, DbType.String);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelBrand>("sp_CreateBrand", dp_param);
            if (result.Code == 200)
            {
                return Ok(new { data = brand });
            }

            return BadRequest(result);
        }

        [Microsoft.AspNetCore.Mvc.HttpPut("Update")]
        [Authorize(Roles = "Admin")]
        public IActionResult Update([System.Web.Http.FromBody] ModelBrand brand, string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("ID", id, DbType.String);
            dp_param.Add("name", brand.Name, DbType.String);
            dp_param.Add("address", brand.Address, DbType.String);
            dp_param.Add("telephone", brand.Telephone, DbType.String);
            dp_param.Add("email", brand.Email, DbType.String);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelBrand>("sp_UpdateBrand", dp_param);
            if (result.Code == 200)
            {
                return Ok(new { data = brand });
            }

            return BadRequest(result);
        }
        [Microsoft.AspNetCore.Mvc.HttpDelete("Delete")]
        [Authorize(Roles = "Admin")]
        public IActionResult Delete(string id)
        {
            if (id == string.Empty)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("ID", id, DbType.String);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelBrand>("sp_DeleteBrand", dp_param);

            if (result.Code == 200)
            {
                return Ok(result);
            }

            return NotFound(result);
        }
    }
}
